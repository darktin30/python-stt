from kivy.lang import Builder
from kivy.uix.screenmanager import Screen


class AboutScreen(Screen):
    def __init__(self, **kwargs):
        super(AboutScreen, self).__init__(**kwargs)
        Builder.load_file('Screens/AboutScreen-Kivy.kv')

    def buy_beer(self):
        pass

