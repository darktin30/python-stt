import os
import concurrent.futures as cf
from datetime import datetime
from pydub import AudioSegment

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from functools import partial
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder
from kivy.uix.recycleview import RecycleView
from kivy.uix.textinput import TextInput
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.uix.popup import Popup

from transcriber import SpeechToText
from widgets import AudioSrollerWidget

class TranscriberScreen(Screen):

    def __init__(self, **kwargs):
        super(TranscriberScreen, self).__init__(**kwargs)
        Builder.load_file('screens/TranscriberScreen-Kivy.kv')

        self.stt = SpeechToText(printer=self.print_text)
        self.input_path = ''
        self.time_format = "%d-%m-%y %H-%M-%S "
        self.parts_folder = 'parts'
        self.transcription_folder = 'transcriptions'
        self.text_box = ''

        Window.bind(on_dropfile=self.on_file_drop)

    def on_file_drop(self, window, file_path):
        """ Handles drag and drop  of input file """
        self.ids.input_path.text = file_path
        self.set_input_path()

    def print_text(self, text):
        """ Prints text to main text label """
        s = '>'
        self.text_box += f'{s} {text}\n'
        self.ids.text_terminal.text = self.text_box

    def set_input_path(self):
        """ Called by on_click of load button """
        path = self.ids.input_path.text

        if os.path.isfile(path):
            self.input_path = path
            self.input_name = os.path.splitext(os.path.basename(path))
            self.print_text(f'Selected file {self.input_path} for transcription!')
        else:
            self.print_text(f'Invalid path or file type!')

    def load_audio_file(self, path):
        """ Load audio file from path """
        try:
            audio = AudioSegment.from_file(path, os.path.splitext(path)[1].replace(".", ""))
            return audio
        except ImportError:
            self.print_text(f"Error in {self.__name__}: Filetype not supported")
            return None

    def cut_audio_parts(self, audio, time_stamps):
        """ User can pre-cut audio to more manageable parts """
        parts = []
        prev_time = 0
        for curr_time in time_stamps:
            assert(curr_time <= len(audio))
            parts.append(audio[prev_time:curr_time])
            prev_time = curr_time

        return parts

    def store_audio_parts(self, parts):
        """ Store pre-cut audio parts """
        for index, parts in enumerate(parts):
            parts.export(os.path.join(self.parts_folder, f'{self.input_name}-parts-{index:003}.wav'), format="wav")

    def write_transcription(self, entire_text):
        """ Write transcription to file """

        """ Create filename """
        now = datetime.now()
        time = now.strftime(self.time_format)
        filename = f'transcription-{time}.txt'

        """ Make output directory """
        if not os.path.isdir(self.transcription_folder):
            os.mkdir(self.transcription_folder)

        file_path = os.path.join(self.transcription_folder, filename)

        """ Write output to file """
        file = open(file_path, 'w+')
        file.write(entire_text)
        file.close()

        self.print_text(f'Wrote text to: {file_path}!')

    def transcribe(self, cache=True):
        """ Text transcription """
        if not os.path.isfile(self.input_path):
            """ Error notification popup """
            error_msg = 'Please set the input file first!\nEither write the path into the dedicated input field\n'
            error_msg += 'or drag and drop it in to the application!'
            popup = Popup(title='Error',
                          content=Label(text=error_msg),
                          size_hint=(None, None), size=(400, 400))

            popup.open()

            return

        """ Get audio file from path """
        audio = self.load_audio_file(self.input_path)

        """ Get time_stamps on where to cut audio """
        # FIXME: Pydub does things in milliseconds ~> 1s = 1000ms
        #time_stamps = [(540+75)*1000, (1200+2)*1000, (1740+3)*1000, len(audio)]  # TODO
        time_stamps = [len(audio)]

        """ Get list of audio_parts according to time_stamps """
        audio_parts = self.cut_audio_parts(audio, time_stamps)

        self.print_text(f'Started transcription!')


        if not cache:
            self.store_audio_parts(audio_parts)
            # TODO: Implement version where parts are stored to disk and stt is called with path

        """ Magic """
        text_data = {}
        with cf.ThreadPoolExecutor(max_workers=2) as executor:
            tc = self.stt.get_audio_transcription
            futures = {executor.submit(tc, index, part): index for index, part in enumerate(audio_parts)}

            for future in cf.as_completed(futures):
                index = futures[future]
                try:
                    given_index, text = future.result()
                    assert(given_index == index)
                except Exception as e:
                    print(f'Audio part {index} generated an exception: {e}')
                else:
                    text_data[index] = text

        entire_text = ' '.join(list(text_data.fromkeys(sorted(text_data.values()))))

        self.write_transcription(entire_text)

        self.print_text(f'Ended transcription!')
