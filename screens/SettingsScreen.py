import os

from kivy.lang import Builder
from kivy.uix.screenmanager import Screen

from configurations import transcriber_config, stt_config


class SettingsScreen(Screen):
    def __init__(self, **kwargs):
        super(SettingsScreen, self).__init__(**kwargs)
        Builder.load_file('Screens/SettingsScreen-Kivy.kv')

    def set_segment_path(self):
        path = self.ids.output_path.text
        if os.path.isdir(path):
            transcriber_config['segment_path'] = path

    def set_output_path(self):
        path = self.ids.output_path.text
        if os.path.isdir(path):
            stt_config['transcription_path'] = path

