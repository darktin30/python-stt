import datetime
import os
import shutil
import speech_recognition as sr
import pydub
import re
from pydub import AudioSegment
from pydub import effects
from pydub.silence import split_on_silence

from configurations import transcriber_config


class SpeechToText:
    printer = None

    def __init__(self, printer=None, config=transcriber_config):
        self.config = config
        self.r = sr.Recognizer()
        self.printer = printer  # TODO: Make thread save

    def split_to_segments(self, audio_part, segment_folder, min_len=40, keep=False):
        if os.path.isdir(segment_folder) and not keep:
            shutil.rmtree(segment_folder)
            os.mkdir(segment_folder)
        if not os.path.isdir(segment_folder):
            os.mkdir(segment_folder)

        audio_part = effects.normalize(audio_part)

        time_codes, chunks = split_on_silence(audio_part,
                                              # experiment with this value for your target audio file
                                              min_silence_len=self.config['split_on_silence']['min_silence_len'],
                                              # adjust this per requirement
                                              silence_thresh=audio_part.dBFS + self.config['split_on_silence'][
                                                  'silence_thresh_offset'],
                                              # keep the silence for 1 second, adjustable as well
                                              keep_silence=self.config['split_on_silence']['keep_silence'],
                                              seek_step=self.config['split_on_silence']['seek_step'],
                                              get_timings=True
                                              )

        file_index = 0
        current_duration = 0
        audio_segment = pydub.AudioSegment.empty()
        print(f'Generated chunks: {len(chunks)}')
        start_of_segment = True
        time_stamp = ''
        for i, audio_chunk in enumerate(chunks, start=1):
            audio_segment += audio_chunk

            if i == len(chunks) or current_duration > (min_len * 1000):
                file_index += 1
                segment_filename = os.path.join(segment_folder, f"segment-index-{file_index:03}-at-{time_stamp}.wav")

                audio_segment.export(segment_filename, format="wav")
                audio_segment = pydub.AudioSegment.empty()

                current_duration = 0
                start_of_segment = True
            else:
                if start_of_segment:
                    time_stamp = f'{time_codes[i][0] // 1000}'
                    #time_stamp = f'{datetime.timedelta(milliseconds=time_codes[i][0])} to {datetime.timedelta(milliseconds=time_codes[i][1])}'
                start_of_segment = False

                current_duration += len(audio_chunk)

    def transcribe_segments(self, segment_folder, verbose=False):
        """ Transcribe all audio segments in segment_folder """

        whole_text = ""
        for segment_file in sorted(os.listdir(segment_folder)):
            segment_path = os.path.join(segment_folder, segment_file)
            with sr.AudioFile(segment_path) as source:
                # FIXME: adjust for ambient noise makes no sense for sample wise translation
                # r.adjust_for_ambient_noise(source, duration=self.config['recognizer']['adjust_for_ambient_noise_duration'])

                audio_data = self.r.record(source)
                time_stamp = re.search('at-(.+?).wav', segment_file).group(1)

                try:
                    text = self.r.recognize_google(audio_data, language=self.config['recognizer']['language'])
                    print(f'{time_stamp}: {text}')
                except Exception as e:
                    text = f"\n Missing {segment_file}\n"
                    print(f"Recognizer returned an error on {segment_file}: {e}")

                if verbose:
                    if self.printer is not None:
                        self.printer(text)
                    else:
                        print(f'Understood: {text}')

                whole_text += f'{time_stamp}: {text}\n'

        return whole_text

    def get_audio_transcription(self, index, source):
        segment_folder = f'segments-{index}'

        audio_part = None

        if type(source) is AudioSegment:
            audio_part = source
        elif type(source) is str and os.path.isfile(source):
            audio_part = AudioSegment.from_wav(source)

        assert (audio_part is not None)

        segment_path = os.path.join(self.config['segment_path'], segment_folder)

        self.split_to_segments(audio_part, segment_path, min_len=10)
        text = self.transcribe_segments(segment_path, verbose=True)

        return index, text


if __name__ == '__main__':
    t = SpeechToText()
    _, output = t.get_audio_transcription(0, 'senf.wav')
    print(f'\nThe transcribed text is: \n{output}')
