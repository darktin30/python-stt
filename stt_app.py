import os
from datetime import datetime
from pydub import AudioSegment

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from functools import partial
from kivy.properties import ObjectProperty
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.lang import Builder
from kivy.uix.recycleview import RecycleView
from kivy.uix.textinput import TextInput
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.uix.popup import Popup
from kivy.lang import Builder

from screens.TranscriberScreen import TranscriberScreen
from screens.SettingsScreen import SettingsScreen
from screens.AboutScreen import AboutScreen

class MenuWidget(GridLayout):
    pass


class SttScreenManager(ScreenManager):
    def __init__(self, **kwargs):
        super(SttScreenManager, self).__init__(**kwargs)


class SttApp(App):
    def __init__(self, **kwargs):
        super(SttApp, self).__init__(**kwargs)

        self.title = 'Speech to text converter'

    def build_settings(self, settings):
        with open('config.json', 'r') as file:
            configdata = file.read()

        settings.add_json_panel('Speech to text', self.config, data=configdata)

    def build(self):

        # TODO: FIXME: Dis is nut warkin gud dahmn

        sm = SttScreenManager(transition=NoTransition())
        sm.add_widget(TranscriberScreen(name='transcriber'))
        sm.add_widget(AboutScreen(name='about'))
        sm.add_widget(SettingsScreen(name='settings'))

        return sm

if __name__ == '__main__':
    SttApp().run()