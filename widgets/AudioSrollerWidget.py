import matplotlib.pyplot as plt
import numpy as np

from scipy.io import wavfile

from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.properties import StringProperty
from kivy.lang import Builder

class AudioScrollerWidget(Widget):

    def __init__(self, **kwargs):
        super(AudioScrollerWidget, self).__init__(**kwargs)
        Builder.load_file('widget/AudioScrollerWidget-Kivy.kv')

        self.audio_file = None

    def set_audio_file(self, path):
        self.audio_file = path

    def display(self):
        fs, x = wavfile.read(self.audio_file)
        y = np.linspace(0, len(x) / float(fs), len(x))
        ya = np.max(np.absolute(x))
        plt.plot(y, x, color="#004225")
        plt.xlabel("Time (seconds)")
        plt.ylabel("Amplitude")
        plt.ylim(-ya, ya)
        plt.xlim(0, y[-1])
        plt.show()

if __name__ == "__main__":
    self.audio_file


