
transcriber_config = {
        'split_on_silence':{ # settings for split_on_silence()
            'min_silence_len':1000, # (in ms) minimum length of a silence to be used for a split. default: 1000ms
            'silence_thresh_offset':-20, # anything quieter than this will be considered silence. default: -16dBFS
            'keep_silence':500, # (in ms or True/False) leave some silence at the beginning and end of the chunks.
            'seek_step':100, # step size for iterating over the segment in ms. default: 100ms
            },
        'split_to_segments':{ # settings for split_to_segments()
            'min_len':15, # (in s) minimum length of concatenated voice segments
            },
        'recognizer':{ # settings for SpeechRecognition Recognizer() functions
            'adjust_for_ambient_noise_duration':0.5, # (in s) amount of start of recording that can be used to remove noise
            'language':'de-DE',# language for translation
            },
        'segment_path':'.'
        }

stt_config = {
        'transcription_path':'.'
}